package com.example.testcalendar;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CalendarItemAdapter extends BaseAdapter {

    private ArrayList<CalendarItem> calendarItems;
    private Context context;
    private ArrayList<DayCountBean> countBeans;

    private LinearLayout ll_calendarItem;
    private LinearLayout ll_reserve;
    private LinearLayout ll_receipt;
    private LinearLayout ll_prescription;
    private TextView tv_date;
    private TextView tv_reserve;
    private TextView tv_receipt;
    private TextView tv_prescription;

    public CalendarItemAdapter(Context context, ArrayList<CalendarItem> calendarItems) {
        this.context = context;
        this.calendarItems = calendarItems;
//        Log.e("ItemAdapter", "calendarItems.size: " + calendarItems.size());
    }

    @Override
    public int getCount() {
        return calendarItems.size();
    }

    @Override
    public Object getItem(int position) {
        return calendarItems.get(position).getId();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.calendar_item, parent, false);
        }

        CalendarItem day = calendarItems.get(position);

        ll_calendarItem = convertView.findViewById(R.id.ll_calendarItem);
        ll_reserve = convertView.findViewById(R.id.ll_reserve);
        ll_receipt = convertView.findViewById(R.id.ll_receipt);
        ll_prescription = convertView.findViewById(R.id.ll_prescription);
        tv_date = convertView.findViewById(R.id.tv_date);
        tv_reserve = convertView.findViewById(R.id.tv_reserve);
        tv_receipt = convertView.findViewById(R.id.tv_receipt);
        tv_prescription = convertView.findViewById(R.id.tv_prescription);

        tv_date.setText(Integer.toString(day.getDay()));
        if (day != null) {
            if (day.isInMonth()) {
                if (position % 7 == 6) {
                    tv_date.setTextColor(Color.parseColor("#E48E8D"));
                } else if (position % 7 == 5) {
                    tv_date.setTextColor(Color.parseColor("#65AFEC"));
                } else {
                    tv_date.setTextColor(Color.parseColor("#333333"));
                }
            } else {
                tv_date.setTextColor(Color.parseColor("#D4D4D4"));
            }
            if (day.isChanged()) {
                ll_calendarItem.setBackgroundColor(Color.YELLOW);
                calendarItems.get(position).setChanged(false);
                calendarItems.get(position).setDay(day.getId() % 100);
//                Log.e("ItemAdapter", "position" + calendarItems.get(position).getId() + "isChanged : " + calendarItems.get(position).isChanged());
            } else {
                ll_calendarItem.setBackgroundColor(Color.WHITE);
            }
//            Log.e("ItemAdapter", "receiptCount: " + day.getReceiptCount());

            if (day.isReceipt()) {
                ll_receipt.setVisibility(View.VISIBLE);
                tv_receipt.setText(day.getReceiptCount());
                Log.e("ItemAdapter", "receiptDay: " + day.getId());
            } else {
                ll_receipt.setVisibility(View.GONE);
            }

            if (day.isReserve()) {
                ll_reserve.setVisibility(View.VISIBLE);
                tv_reserve.setText(day.getReserveCount());
            } else {
                ll_reserve.setVisibility(View.GONE);
            }

            if (day.isPrescript()) {
                ll_prescription.setVisibility(View.VISIBLE);
                tv_prescription.setText(day.getPrescriptCount());
                Log.e("ItemAdapter", "PrescriptDay: " + day.getId());
            } else {
                ll_prescription.setVisibility(View.GONE);
            }
        }


        return convertView;
    }

    public final class CategoryType {
        public static final int RECEIPT = 0;
        public static final int RESERVE = 1;
        public static final int PRESCRIPT = 2;
    }

    public void updateCalendarItem(int id, int count) {
        Log.e("Adapter", "count: " + count);
        Log.e("Adapter", "id: " + id);

        for (int i = 0; i < this.calendarItems.size(); i++) {
            if (calendarItems.get(i).getId() == id) {
                calendarItems.get(i).setDay(count);
                calendarItems.get(i).setChanged(true);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
//        tv_date.setBackgroundColor(Color.WHITE);
    }

    public void setCountBeans(ArrayList<DayCountBean> countBeans) {
        this.countBeans = countBeans;
        Log.e("ItemAdapter", "dayCountBeans: " + countBeans.toString());
        if (countBeans != null) {
            for (int j = 0; j < countBeans.size(); j++) {
                Log.e("ItemAdapter", "dayCountBeans: " + countBeans.get(j).toString());
                for (int i = 0; i < this.calendarItems.size(); i++) {
                    if (calendarItems.get(i).getId() == Integer.parseInt(countBeans.get(j).getDate())) {
                        if(countBeans.get(j).getType() == CategoryType.RECEIPT) {
                            calendarItems.get(i).setType(countBeans.get(j).getType());
                            calendarItems.get(i).setReceiptCount(countBeans.get(j).getCount());
                            calendarItems.get(i).setReceipt(true);
                        } else if(countBeans.get(j).getType() == CategoryType.RESERVE) {
                            calendarItems.get(i).setType(countBeans.get(j).getType());
                            calendarItems.get(i).setReserveCount(countBeans.get(j).getCount());
                            calendarItems.get(i).setReserve(true);
                        } else {
                            calendarItems.get(i).setType(countBeans.get(j).getType());
                            calendarItems.get(i).setPrescriptCount(countBeans.get(j).getCount());
                            calendarItems.get(i).setPrescript(true);
                        }
//                        switch (countBeans.get(j).getType()) {
//                            case CategoryType.RECEIPT:
//                                calendarItems.get(i).setType(countBeans.get(j).getType());
//                                calendarItems.get(i).setReceiptCount(countBeans.get(j).getCount());
//                                calendarItems.get(i).setReceipt(true);
//
//                                break;
//                            case CategoryType.RESERVE:
//                                calendarItems.get(i).setType(countBeans.get(j).getType());
//                                calendarItems.get(i).setReserveCount(countBeans.get(j).getCount());
//                                calendarItems.get(i).setReserve(true);
//                                break;
//                            case CategoryType.PRESCRIPT:
//                                calendarItems.get(i).setType(countBeans.get(j).getType());
//                                calendarItems.get(i).setPrescriptCount(countBeans.get(j).getCount());
//                                calendarItems.get(i).setPrescript(true);
//                                break;
//                        }
                    }
                }
            }

        }
        notifyDataSetChanged();
    }


    public void setCountClear() {
        for (int i = 0; i < this.calendarItems.size(); i++) {
            calendarItems.get(i).setAllCount(null);
            calendarItems.get(i).setAllBoolean(false);
        }
        notifyDataSetChanged();
    }

}
