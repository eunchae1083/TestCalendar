package com.example.testcalendar;

public class CalendarItem {
    private int id;
    private int day;
    private boolean isInMonth = true;
    private String receiptCount;
    private String prescriptCount;
    private String reserveCount;
    private boolean isReserve = false;
    private boolean isReceipt = false;
    private boolean isPrescript = false;
    private int type;
    private boolean isChanged = false;

    public void setAllCount(String count) {
        this.receiptCount = count;
        this.reserveCount = count;
        this.prescriptCount = count;
    }

    public void setAllBoolean(Boolean value){
        this.isReceipt = value;
        this.isReserve = value;
        this.isPrescript = value;
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(boolean changed) {
        isChanged = changed;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public boolean isInMonth() {
        return isInMonth;
    }

    public void setInMonth(boolean inMonth) {
        isInMonth = inMonth;
    }

    public String getReceiptCount() {
        return receiptCount;
    }

    public void setReceiptCount(String receiptCount) {
        this.receiptCount = receiptCount;
    }

    public String getPrescriptCount() {
        return prescriptCount;
    }

    public void setPrescriptCount(String prescriptCount) {
        this.prescriptCount = prescriptCount;
    }

    public String getReserveCount() {
        return reserveCount;
    }

    public void setReserveCount(String reserveCount) {
        this.reserveCount = reserveCount;
    }

    public boolean isReserve() {
        return isReserve;
    }

    public void setReserve(boolean reserve) {
        isReserve = reserve;
    }

    public boolean isReceipt() {
        return isReceipt;
    }

    public void setReceipt(boolean receipt) {
        isReceipt = receipt;
    }

    public boolean isPrescript() {
        return isPrescript;
    }

    public void setPrescript(boolean prescript) {
        isPrescript = prescript;
    }
}
