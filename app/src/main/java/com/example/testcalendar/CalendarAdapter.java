package com.example.testcalendar;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolder> {
    public interface OnCalendarAdapterListener {
        void onClick(String id);
    }

    private Context context;
    private ArrayList<ArrayList<CalendarItem>> calendarItemList = new ArrayList<>();
    private int calendarMaxSize = 50;
    final int start_position = calendarMaxSize / 2;
    private final int year;
    private final int month;
    private Map<Integer, String> titleMap = new HashMap<>();

    private int currentCalendar;
    private ArrayList<DayCountBean> countBeans = new ArrayList<>();

    private OnCalendarAdapterListener onCalendarAdapterListener = null;

    ArrayList<MakeCalendar> makeCalendar = new ArrayList<>();

    public void setOnCalendarAdapterListener(OnCalendarAdapterListener onCalendarAdapterListener) {
        this.onCalendarAdapterListener = onCalendarAdapterListener;
    }

    public CalendarAdapter(Context context, int year, int month) {
        this.context = context;
        this.year = year;
        this.month = month;

        for (int i = 0; i < calendarMaxSize; i++) {
            makeCalendar.add(new MakeCalendar(context));
        }
    }

    public CalendarAdapter(Context context, int year, int month, ArrayList<DayCountBean> countBeans) {
        this.context = context;
        this.year = year;
        this.month = month;
        this.countBeans = countBeans;

        for (int i = 0; i < calendarMaxSize; i++) {
            makeCalendar.add(new MakeCalendar(context));
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == start_position) {
            holder.makeCalendar = new MakeCalendar(context, year, holder.calendar.get(Calendar.MONTH) + (position - start_position), countBeans);
        } else {
            holder.makeCalendar = new MakeCalendar(context, year, holder.calendar.get(Calendar.MONTH) + (position - start_position));
        }
        makeCalendar.set(position, holder.makeCalendar);

        holder.gv_calendar.setAdapter(makeCalendar.get(position).getCalendarItemAdapter());

        titleMap.put(position, holder.makeCalendar.getTitle());

    }

    @Override
    public int getItemCount() {
        return calendarMaxSize;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private GridView gv_calendar;

        private MakeCalendar makeCalendar;
        private Calendar calendar = Calendar.getInstance();

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            gv_calendar = itemView.findViewById(R.id.gv_calendar);

        }
    }

    public String getTitle(int currentCalendar) {
        this.currentCalendar = currentCalendar;
        return titleMap.get(currentCalendar);
    }

    public int getCurrent() {
        return Integer.parseInt(String.valueOf(makeCalendar.get(currentCalendar).getCal().get(Calendar.MONTH + 1)));
    }

    public void updateCalendarItem(ArrayList<DayCountBean> dayCountBeans) {
        if (dayCountBeans != null) {
//            int prevMonth =Integer.parseInt(String.valueOf(makeCalendar.get(currentCalendar).getCal().get(Calendar.MONTH )));
//            int nextMonth =Integer.parseInt(String.valueOf(makeCalendar.get(currentCalendar).getCal().get(Calendar.MONTH + 2)));

            makeCalendar.get(currentCalendar).getCalendarItemAdapter()
                    .setCountBeans(dayCountBeans);

        }
    }

    public int dateToMonthValue(String strDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat to_sdf = new SimpleDateFormat("MM");
        try {
            Date date = sdf.parse(strDate);
            return Integer.parseInt(to_sdf.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

    }
}