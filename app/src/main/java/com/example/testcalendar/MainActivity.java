package com.example.testcalendar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private Context context;

    private TextView tv_title;
    private ViewPager2 vp_calendar;
    private Button btn_a;
    private Button btn_b;
    private Button btn_c;
    private Button btn_d;
    private Button btn_e;
    private Button btn_receipt;
    private Button btn_reserve;
    private Button btn_prescript;
    private EditText et_count;
    private EditText et_date;
    private CalendarAdapter calendarAdapter;

    private int year;
    private int month;
    private int currentCalendar;

    private ArrayList<DayCountBean> dayCountBeans = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;

        tv_title = findViewById(R.id.tv_title);
        vp_calendar = findViewById(R.id.vp_calendar);

        dayCountBeans.add(new DayCountBean("20211220","7", CalendarItemAdapter.CategoryType.RECEIPT));
        dayCountBeans.add(new DayCountBean("20220120","2", CalendarItemAdapter.CategoryType.RESERVE));
        dayCountBeans.add(new DayCountBean("20220320","2", CalendarItemAdapter.CategoryType.PRESCRIPT));

        LocalDate now = LocalDate.now();
        year = now.getYear();
        month = now.getMonthValue();

        calendarAdapter = new CalendarAdapter(context, year, month, dayCountBeans);
        vp_calendar.setAdapter(calendarAdapter);
        vp_calendar.setCurrentItem(calendarAdapter.start_position, false);

        calendarAdapter.setOnCalendarAdapterListener(new CalendarAdapter.OnCalendarAdapterListener() {
            @Override
            public void onClick(String id) {
//                et_date.setText(id);
            }
        });

        vp_calendar.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                currentCalendar = position;

                tv_title.setText(calendarAdapter.getTitle(currentCalendar));

                calendarAdapter.updateCalendarItem(dayCountBeans);

            }
        });



//        btn_a = findViewById(R.id.btn_a);
//        btn_b = findViewById(R.id.btn_b);
//        btn_c = findViewById(R.id.btn_c);
//        btn_d = findViewById(R.id.btn_d);
//        btn_e = findViewById(R.id.btn_e);
//        btn_receipt = findViewById(R.id.btn_receipt);
//        btn_reserve = findViewById(R.id.btn_reserve);
//        btn_prescript = findViewById(R.id.btn_prescript);
//        et_count = findViewById(R.id.et_count);
//        et_date = findViewById(R.id.et_date);
//
//        btn_a.setText("저번달");
//        btn_b.setText("이번달");
//        btn_c.setText("다음달");
//        btn_d.setText("setDay");
//        btn_e.setText("setCount");
//
//        btn_a.setOnClickListener(onClickListener);
//        btn_b.setOnClickListener(onClickListener);
//        btn_c.setOnClickListener(onClickListener);
//        btn_d.setOnClickListener(onClickListener);
//        btn_e.setOnClickListener(onClickListener);
//        btn_receipt.setOnClickListener(onClickListener);
//        btn_reserve.setOnClickListener(onClickListener);
//        btn_prescript.setOnClickListener(onClickListener);
//

    }

//    private View.OnClickListener onClickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//
//            switch (view.getId()) {
//                case R.id.btn_a:
//                    makeCalendar.setNewCalendar(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)-1);
//                    calendarItemAdapter.setCountClear();
//                    tv_title.setText(makeCalendar.getTitle());
//                    break;
//                case R.id.btn_b:
//                    makeCalendar.setNewCalendar(year, month-1);
////                        calendarItemAdapter.notifyDataSetChanged();
//                    calendarItemAdapter.setCountClear();
//                    tv_title.setText(makeCalendar.getTitle());
//                    break;
//                case R.id.btn_c:
//                    makeCalendar.setNewCalendar(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1);
//                    calendarItemAdapter.setCountClear();
//                    dayCountBeans.clear();
//                    tv_title.setText(makeCalendar.getTitle());
//                    break;
//
//                case R.id.btn_d:
//                    if(et_count.getText().toString().length() >0 && et_date.getText().toString().length()>0) {
//
//                        int count = Integer.parseInt(et_count.getText().toString());
//                        int id = Integer.parseInt(et_date.getText().toString());
//                        Log.e("Main","count: " + count);
//                        Log.e("Main","id: " + id);
//
//                        calendarItemAdapter.updateCalendarItem(id, count);
////                        calendarItemAdapter.notifyDataSetChanged();
//                    }
//                    break;
//                case R.id.btn_e:
//
//                    Log.e("Main","dayCountBeans.size: " + dayCountBeans.size());
//                    calendarItemAdapter.setCountBeans(dayCountBeans);
//                    break;
//                case R.id.btn_receipt:
//                    dayCountBeans.add(new DayCountBean(et_date.getText().toString(), et_count.getText().toString(), CalendarAdapter.CategoryType.RECEIPT));
//                    break;
//                case R.id.btn_reserve:
//                    dayCountBeans.add(new DayCountBean(et_date.getText().toString(), et_count.getText().toString(), CalendarAdapter.CategoryType.RESERVE));
//                    break;
//                case R.id.btn_prescript:
//                    dayCountBeans.add(new DayCountBean(et_date.getText().toString(), et_count.getText().toString(), CalendarAdapter.CategoryType.PRESCRIPT));
//                    break;
//            }
//
//        }
//    };

}