package com.example.testcalendar;

import android.content.Context;
import android.util.Log;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.Calendar;

public class MakeCalendar extends GridView {

    private int month;
    private int year;
    private int lastDays;
    private int prevMonthTailOffset;
    private int nextMonthHeadOffset;

    private int index;

    private CalendarItemAdapter calendarItemAdapter;

    private Calendar cal = Calendar.getInstance();
    private Calendar prevCal;

    private int id;
    private int prev_id;

    private ArrayList<CalendarItem> calendarItems = new ArrayList<CalendarItem>();
    private static int row_of_week = 6;
    private static int day_of_week = 7;

    private ArrayList<DayCountBean> countBeans = new ArrayList<>();

    public MakeCalendar(Context context) {
        super(context);
    }

    public MakeCalendar(Context context, int year, int month) {
        super(context);

        this.year = year;
        this.month = month;

        draw(year, month);
    }

    public MakeCalendar(Context context, int year, int month, ArrayList<DayCountBean> countBeans) {
        super(context);

        this.year = year;
        this.month = month;
        this.countBeans = countBeans;

        draw(year, month);
    }

    public void draw(int year, int month) {
        index = 0;
        calendarItems.clear();

        cal.set(year, month, 1);
        lastDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        prevMonthTailOffset = cal.get(Calendar.DAY_OF_WEEK) - 2; //월요일 부터 시작

        prevCal = (Calendar) cal.clone();
        prevCal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 1, 1);
        int prevLastDay = prevCal.getActualMaximum(Calendar.DAY_OF_MONTH);

        if (prevMonthTailOffset > 0) {
            for (int i = prevMonthTailOffset - 1; i >= 0; i--) {
                CalendarItem day = new CalendarItem();
                day.setDay(prevLastDay - i);
                day.setInMonth(false);
                calendarItems.add(day);

                prev_id = (prevCal.get(Calendar.YEAR) * 10000) + ((prevCal.get(Calendar.MONTH) + 1) * 100) + (prevLastDay - i);
                calendarItems.get(index).setId(prev_id);
//                Log.e("MakeCalendar", "prev_id: " + prev_id);

                index++;
            }
//            Log.e("MakeCalendar","calendarItems: " + calendarItems.size());
        }

        for (int i = 1; i < lastDays + 1; i++) {
            CalendarItem day = new CalendarItem();
            day.setDay(i);
            day.setInMonth(true);
            calendarItems.add(day);
            id = (cal.get(Calendar.YEAR) * 10000) + (cal.get(Calendar.MONTH) + 1) * 100 + i;
            calendarItems.get(index).setId(id);
            Log.e("MakeCalendar", "id: " + id);
            index++;
        }
//        Log.e("MakeCalendar","calendarItems: " + calendarItems.size());

        if (prevMonthTailOffset <= 0) {
            nextMonthHeadOffset = (row_of_week * day_of_week) - (lastDays);
        } else {
            nextMonthHeadOffset = (row_of_week * day_of_week) - (prevMonthTailOffset + lastDays);
        }

//        Log.e("MakeCalendar","nextMonthHeadOffset: " + nextMonthHeadOffset);


        for (int i = 1; i < nextMonthHeadOffset + 1; i++) {
            CalendarItem day = new CalendarItem();
            day.setDay(i);
            day.setInMonth(false);
            calendarItems.add(day);
            if (cal.get(Calendar.MONTH) < 11) {
                calendarItems.get(index).setId((cal.get(Calendar.YEAR) * 10000) + (cal.get(Calendar.MONTH) + 2) * 100 + i);
//                Log.e("MakeCalendar", "next_id: " + (cal.get(Calendar.YEAR) * 10000 + (cal.get(Calendar.MONTH) + 2) * 100 + i));
            } else {
                calendarItems.get(index).setId((((cal.get(Calendar.YEAR) + 1) * 10000) + (1) * 100 + i));
//                Log.e("MakeCalendar", "next_id: " + (((cal.get(Calendar.YEAR) + 1) * 10000) + (1) * 100 + i));
            }
            index++;

        }
//        Log.e("MakeCalendar", "calendarItems: " + calendarItems.size());

        if (countBeans != null) {
            setCountBeans();
        }
        calendarItemAdapter = new CalendarItemAdapter(getContext(), calendarItems);

    }

    public CalendarItemAdapter getCalendarItemAdapter() {
        return calendarItemAdapter;
    }

    public Calendar getCal() {
        return cal;
    }


    public String getTitle() {
        return cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1);

    }

    public int getMonthValue() {
        return cal.get(Calendar.MONTH) + 1;
    }

    public void setCountBeans() {
        for (int j = 0; j < countBeans.size(); j++) {
            Log.e("MakeCalendar", "dayCountBeans: " + countBeans.get(j).toString());
            for (int i = 0; i < this.calendarItems.size(); i++) {
                if (calendarItems.get(i).getId() == Integer.parseInt(countBeans.get(j).getDate())) {
                    Log.e("MakeCalendar", "countBeans.get(j).getType(): " + countBeans.get(j).getType());
                    if (countBeans.get(j).getType() == CategoryType.RECEIPT) {
                        calendarItems.get(i).setType(countBeans.get(j).getType());
                        calendarItems.get(i).setReceiptCount(countBeans.get(j).getCount());
                        calendarItems.get(i).setReceipt(true);
                    } else if (countBeans.get(j).getType() == CategoryType.RESERVE) {
                        calendarItems.get(i).setType(countBeans.get(j).getType());
                        calendarItems.get(i).setReserveCount(countBeans.get(j).getCount());
                        calendarItems.get(i).setReserve(true);
                    } else {
                        calendarItems.get(i).setType(countBeans.get(j).getType());
                        calendarItems.get(i).setPrescriptCount(countBeans.get(j).getCount());
                        calendarItems.get(i).setPrescript(true);
                    }

                }
            }
        }
    }

    public final class CategoryType {
        public static final int RECEIPT = 0;
        public static final int RESERVE = 1;
        public static final int PRESCRIPT = 2;
    }
}
