package com.example.testcalendar;

public class DayCountBean {
    private String Date;
    private String Count;
    private int type;

    public DayCountBean(String date, String count, int type) {
        Date = date;
        Count = count;
        this.type = type;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "DayCountBean{" +
                "Date='" + Date + '\'' +
                ", Count='" + Count + '\'' +
                ", type=" + type +
                '}';
    }
}
